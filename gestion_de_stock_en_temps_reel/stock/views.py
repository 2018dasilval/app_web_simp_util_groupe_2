from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from stock.models import Fut
from stock.models import Gaz


def index(request): #page d'accueil
    template = loader.get_template("./index.html") #importe la template associée
    message=template.render(request=request)
    return HttpResponse(message) #renvoie la template d'accueil


def stock(request): # renvoie page Stock avec tableau des futs pour brancher et retirer
    template= loader.get_template("./stock.html") #importe la template associée
    fut = Fut.objects.all() # on importe base de donnée dans la liste fut
    context={'fut': fut}  # dictionnaire pour remplir la template associée
    message = template.render(context,request=request) 
    return HttpResponse(message) 

def brancher(request, fut_id): #déplace un fut plein vers les futs entamés
    template= loader.get_template("./retour_stock.html") #importe la template associée
    fut = Fut.objects.get(id=fut_id) #on fait des changements pour un seul type de fut
    fut.nb_plein -= 1
    fut.nb_entame += 1
    fut.save() #on sauvegarde les changements
    return HttpResponse (template.render(request=request))

def retirer(request, fut_id): #déplace un fut entamé vers les futs vides 
    template=loader.get_template("./retour_stock.html")
    fut = Fut.objects.get(id=fut_id)
    fut.nb_entame -=1
    fut.nb_vide +=1
    fut.save()
    return HttpResponse(template.render(request=request))

def gaz(request): #renvoie la page Gaz qui contient l'inventaire des bouteilles de gaz, on peut brancher et retirer également
    template= loader.get_template("./gaz.html")
    gaz = Gaz.objects.all() # on importe base de donnée dans la liste gaz
    context={'gaz':gaz} #dictionnaire pour remplir la template
    message= template.render(context,request=request) 
    return HttpResponse(message)


def vide(request): #renvoie la page Futs-Vides avec un tableau du nombre de futs vides par volume de fut
    template=loader.get_template("./vide.html")
    volume_30 =Fut.objects.filter(volume=30) #liste de futs dont le volume est 30
    volume_50=Fut.objects.filter(volume=50) #liste de futs dont le volume est 50
    quantite_30= sum([volume.nb_vide for volume in volume_30]) #quantité totale de futs vides avec un volume 30
    quantite_50= sum([volume.nb_vide for volume in volume_50])#quantité totale de futs vides avec un volume 50
    context={'quantite_30':quantite_30,'quantite_50':quantite_50} #dictionnaire pour identifier les valeurs quantite_30/50 dans la template
    message= template.render(context,request=request)
    return HttpResponse(message)

def commandes (request): #renvoie page Commandes permettant d'ajouter des bouteilles de gaz et des futs pleins lors d'une commande
    template= loader.get_template("./commandes.html")
    fut = Fut.objects.all() #on importe la base de donnée des futs
    gaz = Gaz.objects.all() #on importe la base de donnée des gaz
    context={'gaz':gaz,'fut':fut} #dictionnaire
    message = template.render(context,request=request)
    return HttpResponse(message)

def ajouts(request,fut_id): #fonction pour ajouter le nombre voulu de futs pleins lors d'une commande pour un type de fut précis (fut_id)
    query = request.GET.get('query') #on recupère le nombre voulu par l'utilisateur 
    test=True    #on s'assure que la valeur entrée par l'utilisateur est un nombre entier
    for chiffre in query:
        if chiffre not in '1234567890': test=False
    if test==False:
        template=template=loader.get_template("./erreur.html")
        return HttpResponse(template.render(request=request))
    else :
        query=int(query)
        template=loader.get_template("./retour_commande.html")
        query=int(query) 
        fut= Fut.objects.get(pk=fut_id)
        fut.nb_plein+=query #on rajoute le nombre voulu de futs pleins
        fut.save()
        return HttpResponse(template.render(request=request))

def retirer_gaz(request): #fonction qui déplace une bouteille de gaz entamée vers les bouteilles vides
    template=loader.get_template("./retour_gaz.html")
    gaz=Gaz.objects.all()[0]
    gaz.nb_vide+=1
    gaz.nb_entame-=1
    gaz.save()
    return HttpResponse(template.render(request=request))

def brancher_gaz(request): #fonction qui déplace un bouteille de gaz pleine vers les bouteilles entamées
    template=loader.get_template("./retour_gaz.html")
    gaz=Gaz.objects.all()[0]
    gaz.nb_plein-=1
    gaz.nb_entame+=1
    gaz.save()
    return HttpResponse(template.render(request=request))

def ajouts_gaz(request): #fonction pour ajouter le nombre voulu de bouteilles de gaz pleines
    query = request.GET.get('query') #on importe la valeur indiquée par l'utilisateur
    test=True       #la valeur est-elle un nombre entier?  
    for chiffre in query :
        if chiffre not in '1234567890': test=False
    if test==False :
        template=loader.get_template("./erreur.html")
        return HttpResponse(template.render(request=request))

    else:
        template=loader.get_template("./retour_commande.html")
        query=int(query) 
        gaz= Gaz.objects.all()[0] #on importe dans 'gaz' la base de donnée
        gaz.nb_plein+=query 
        gaz.save()
        return HttpResponse(template.render(request=request))

def prix (request): #renvoie la page Prix avec un tableau des tarifs des différentes bières
    template= loader.get_template("./prix.html")
    futs = Fut.objects.all()
    context = { 'futs' : futs }
    return HttpResponse(template.render(context, request=request))
