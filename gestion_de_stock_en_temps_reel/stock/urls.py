from django.conf import settings
from django.conf.urls import include, url  # For django versions before 2.0
from django.urls import path ,re_path
from stock import views

urlpatterns=[
    path('', views.stock, name='stock'),
    path('', views.vide, name='vide'),
    path('', views.gaz, name='gaz'),
    path('', views.commandes, name='commandes'),
    re_path(r'^stock/brancher/(?P<fut_id>[0-9]+)/$', views.brancher, name='brancher'),
    re_path(r'^stock/retirer/(?P<fut_id>[0-9]+)/$', views.retirer, name='retirer'),
    re_path(r'^commandes/ajouts/(?P<fut_id>[0-9]+)/$', views.ajouts, name='ajouts'),
]