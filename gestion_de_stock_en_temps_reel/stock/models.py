from django.db import models

# Create your models here.

class Fut(models.Model):
    marque = models.CharField(max_length=100)
    nb_plein = models.IntegerField()
    nb_entame = models.IntegerField()
    nb_vide = models.IntegerField()
    volume = models.IntegerField()
    TAV = models.FloatField()
    prix_HT = models.FloatField()
    prix_TTC = models.FloatField()
    prix_reel = models.FloatField()
    prix_pinte = models.FloatField()
    prix_vente_Musee = models.FloatField(default=None)
    class Meta:
        unique_together = ("marque", "volume")


class Gaz(models.Model):
    nb_plein = models.IntegerField()
    nb_entame = models.IntegerField()
    nb_vide = models.IntegerField()