from django.urls import resolve
from django.test import TestCase
from stock.views import *
from stock.models import *
from django.template import loader
from django.http import HttpRequest

# Create your tests here.


class WelcomePageTest(TestCase):

    def test_root_url_resolves_to_welcome_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_welcome_page_returns_correct_html(self):
        request = HttpRequest()
        response = index(request)
        html = response.content.decode('utf8')
        template = loader.get_template("./index.html")
        message=template.render(request=request)
        self.assertTrue(html==message)

    def test_page_stock(self):
        request = HttpRequest()
        response = stock(request)
        html = response.content.decode('utf8')
        template= loader.get_template("./stock.html")
        fut = Fut.objects.all()
        context={'fut': fut}
        message = template.render(context,request=request)
        self.assertTrue(html==message)

    def test_page_prix(self):
        request = HttpRequest()
        response = prix(request)
        html = response.content.decode('utf8')
        template= loader.get_template("./prix.html")
        futs = Fut.objects.all()
        context = { 'futs' : futs }
        message = template.render(context, request=request)
        self.assertTrue(html==message)

    def test_page_commandes(self):
        request = HttpRequest()
        response = commandes(request)
        html = response.content.decode('utf8')
        template= loader.get_template("./commandes.html")
        fut = Fut.objects.all()
        gaz = Gaz.objects.all()
        context={'gaz':gaz,'fut':fut}
        message = template.render(context,request=request)
        self.assertTrue(html==message)

    def test_page_gaz(self):
        request = HttpRequest()
        response = gaz(request)
        html = response.content.decode('utf8')
        template= loader.get_template("./gaz.html")
        gazbd = Gaz.objects.all()
        context={'gaz':gazbd}
        message= template.render(context,request=request)
        self.assertTrue(html==message)

    def test_page_vide(self):
        request = HttpRequest()
        response = vide(request)
        html = response.content.decode('utf8')
        template=loader.get_template("./vide.html")
        volume_30 =Fut.objects.filter(volume=30)
        volume_50=Fut.objects.filter(volume=50)
        quantite_30= sum([volume.nb_vide for volume in volume_30])
        quantite_50= sum([volume.nb_vide for volume in volume_50])
        context={'quantite_30':quantite_30,'quantite_50':quantite_50}
        message= template.render(context,request=request)
        self.assertTrue(html==message)
    