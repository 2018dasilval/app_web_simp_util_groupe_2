from django.conf import settings
from django.conf.urls import include, url  # For django versions before 2.0
from django.urls import include, path  # For django versions from 2.0 and up
from django.contrib import admin
from django.urls import re_path
from stock import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('stock/', include('stock.urls')),
    path('vide/', views.vide, name='vide'),
    path('gaz/', views.gaz, name='gaz'),
    path('commandes/', views.commandes, name='commandes'),
    re_path(r'^$', views.index, name='index'),
    re_path(r'^stock/brancher/(?P<fut_id>[0-9]+)/$', views.brancher, name='brancher'),
    re_path(r'^stock/retirer/(?P<fut_id>[0-9]+)/$', views.retirer, name='retirer'),
    re_path(r'^commandes/ajouts/(?P<fut_id>[0-9]+)/$', views.ajouts, name='ajouts'),
    re_path(r'^commandes/brancher_gaz/$', views.brancher_gaz, name='brancher_gaz'),
    re_path(r'^commandes/retirer_gaz/$', views.retirer_gaz, name='retirer_gaz'),
    re_path(r'^commandes/ajouts_gaz/$', views.ajouts_gaz, name='ajouts_gaz'),
    re_path('prix/', views.prix, name='prix')
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns
